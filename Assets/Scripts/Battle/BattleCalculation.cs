﻿using UnityEngine;
using System.Collections;
using System;
using ARCardGame.Models.MonstersStatus;
using ARCardGame.Models.Orders;
using ARCardGame.Models.Attacks;


namespace ARCardGame.BattleSystem
{
    public class BattleCalculation
    {

        public static OrderResult HandleOrder(IOrder PerformerOrder, IOrder ReceiverOrder)
        {
            int totalDamageDealt = TotalDamageDealt(PerformerOrder, ReceiverOrder);

            OrderResult result = CreateResult(PerformerOrder.Attack, totalDamageDealt);
            return result;
        }
        /// <summary>
        /// Calculate Totale Damage Dealt.
        /// </summary>
        /// <param name="Actororder"></param>
        /// <param name="ReceiverOrder"></param>
        /// <returns>
        /// The Total of the physical and magical Damage.
        /// </returns>
        private static int TotalDamageDealt(IOrder Actororder, IOrder ReceiverOrder)
        {
            int physicalDamage = CalculatePhysicalDamageDealt(Actororder, ReceiverOrder);
            int magicalDamage = CalculateMagicalDamageDealt(Actororder, ReceiverOrder);

            int totalDamageDealt = physicalDamage + magicalDamage;
            return totalDamageDealt;
        }
        /// <summary>
        /// Calculate The Magical Damage Dealt.
        /// </summary>
        /// <param name="Actororder"></param>
        /// <param name="ReceiverOrder"></param>
        /// <returns>
        /// The total magical damage dealth
        /// If Actororder.Attack.MagicDamage is 0 then skip the calculation and return 0.
        /// else If total Damage dealt <= 0 return 1.
        /// </returns>
        private static int CalculateMagicalDamageDealt(IOrder Actororder, IOrder ReceiverOrder)
        {
            int magicalDamage;
            magicalDamage = Actororder.Attack.MagicDamage;
            if (magicalDamage > 0)
            {
                magicalDamage = magicalDamage + Actororder.MonsterStatus.MagicAttack;
                magicalDamage = magicalDamage - (CalculateMagicalDefenseTotal(ReceiverOrder));
                if (magicalDamage <= 0)
                    magicalDamage = 1;
            }
            return magicalDamage;
        }
        /// <summary>
        /// Calculate Magical Defense Total
        /// </summary>
        /// <param name="ReceiverOrder"></param>
        /// <returns>The total ammount of Magical defense the receiver have.</returns>
        private static int CalculateMagicalDefenseTotal(IOrder ReceiverOrder)
        {
            return ReceiverOrder.MonsterStatus.MagicDefense + ReceiverOrder.MonsterStatus.MagicDefense * Convert.ToInt32(ReceiverOrder.DefenseState);
        }
        /// <summary>
        /// Calculate The Physical Damage Dealt.
        /// </summary>
        /// <param name="Actororder"></param>
        /// <param name="ReceiverOrder"></param>
        /// <returns>
        /// The total magical damage dealth
        /// If Actororder.Attack.PhysicalDamage is 0 then skip the calculation and return 0.
        /// else If total Damage dealt <= 0 return 1.
        /// </returns>
        private static int CalculatePhysicalDamageDealt(IOrder Actororder, IOrder ReceiverOrder)
        {
            int physicalDamage;
            physicalDamage = Actororder.Attack.PhysicalDamage;
            if (physicalDamage > 0)
            {
                physicalDamage = physicalDamage + Actororder.MonsterStatus.PhysicalAttack;
                physicalDamage = physicalDamage - (CalculatePhysicalDefenseTotal(ReceiverOrder));
                if (physicalDamage <= 0)
                    physicalDamage = 1;
            }
            return physicalDamage;
        }
        /// <summary>
        /// Calculate Physical Defense Total
        /// </summary>
        /// <param name="ReceiverOrder"></param>
        /// <returns>The total ammount of Physical Defense the receiver have.</returns>
        private static int CalculatePhysicalDefenseTotal(IOrder ReceiverOrder)
        {
            return ReceiverOrder.MonsterStatus.PhysicalDefense + ReceiverOrder.MonsterStatus.PhysicalDefense * Convert.ToInt32(ReceiverOrder.DefenseState);
        }

        private static OrderResult CreateResult(IAttackModel Attack, int totalDamageDealt)
        {
            OrderResult result = new OrderResult();
            result.TotalDamageDealt = totalDamageDealt;
            result.TotalDamageRecieved = 0;
            result.TotalManaLost = Attack.ManaPrice;
            result.TotalStaminaLost = Attack.StaminaPrice;
            return result;
        }
    }

}
