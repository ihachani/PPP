﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ARCardGame.Managers;
using ARCardGame.Models.MonstersStatus;
using ARCardGame.Models.Orders;

namespace ARCardGame.BattleSystem
{
    public class OrderHandler : MonoBehaviour
    {
        Order player1Order;

        public Order Player1Order
        {
            get { return player1Order; }
            set { player1Order = value; }
        }
        Order player2Order;

        public Order Player2Order
        {
            get { return player2Order; }
            set { player2Order = value; }
        }
        OrderHandlerState state;
        OrderHandlerState waitingForBothState;
        OrderHandlerState waitingForOneState;
        int damageForPlayer1;
        int damageForPlayer2;
        GameManager manager;
        void Start()
        {
            manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
            waitingForBothState = new OrderHandlerWaitingForBothState(this, manager);
            waitingForOneState = new OrderHandlerWaitingForOnePlayerState(this, manager);
            state = waitingForBothState;

        }
        public void HandleOrder(Order order)
        {
            state.HandleOrder(order);
        }

        public void ExecuteOrders()
        {
            //Must refactor this.
            //Todo Refactor this.
            OrderResult player1Result = null;
            OrderResult player2Result = null;
            List<OrderResult> results = new List<OrderResult>();
            if (player1Order.Attack.Name == "Defend")
            {
                player1Result = BattleCalculation.HandleOrder(player1Order, player2Order);

                //results.Add(player1Result);

                GetDefendingStatus(player1Order).DefenseState = true;

                if (player2Order.Attack.Name == "Defend")
                {
                    player2Result = BattleCalculation.HandleOrder(player2Order, player1Order);
                    //results.Add(player2Result);
                    GetDefendingStatus(player2Order).DefenseState = true;
                }
                else
                {
                    player2Result = BattleCalculation.HandleOrder(player2Order, player1Order);
                    //results.Add(player2Result);
                }
            }
            else if (player2Order.Attack.Name == "Defend")
            {
                player2Result = BattleCalculation.HandleOrder(player2Order, player1Order);
                //results.Add(player2Result);
                GetDefendingStatus(player2Order).DefenseState = true;
                player1Result = BattleCalculation.HandleOrder(player1Order, player2Order);
                //results.Add(player1Result);
            }
            else
            {
                player1Result = BattleCalculation.HandleOrder(player1Order, player2Order);
                player2Result = BattleCalculation.HandleOrder(player2Order, player1Order);
            }
            // TODO Change The List To A Dictionary.
            results.Add(player1Result);
            results.Add(player2Result);
            List<Order> orders = new List<Order>();
            orders.Add(player1Order);
            orders.Add(player2Order);
            manager.HandleBattle(results, orders);
            //defending???????
            ChangeStateToWaitingForBoth();

        }

        private MonsterStatus GetMonsterStatus(Order order)
        {
            return order.Charcter.GetComponent<MonsterStatus>();
        }
        private static Defending GetDefendingStatus(Order order)
        {
            return order.Charcter.GetComponent<Defending>();
        }
        public void ChangeStateToWaitingForBoth()
        {
            state = waitingForBothState;
        }
        public void ChangeStateToWaitingForOne()
        {
            state = waitingForOneState;
        }
    }
    abstract class OrderHandlerState
    {
        protected OrderHandler orderHandler;
        protected GameManager manager;
        public OrderHandlerState(OrderHandler orderHandler, GameManager manager)
        {
            this.orderHandler = orderHandler;
            this.manager = manager;
        }
        public virtual void HandleOrder(Order order)
        {

        }
    }

    class OrderHandlerWaitingForBothState : OrderHandlerState
    {
        public OrderHandlerWaitingForBothState(OrderHandler orderHandler, GameManager manager)
            : base(orderHandler, manager)
        {

        }
        public override void HandleOrder(Order order)
        {
            if (order.Player == 1)
            {
                orderHandler.Player1Order = order;
                manager.FreezePlayer1UI();
            }
            else
            {
                orderHandler.Player2Order = order;
                manager.FreezePlayer2UI();
            }
            orderHandler.ChangeStateToWaitingForOne();
        }
    }
    class OrderHandlerWaitingForOnePlayerState : OrderHandlerState
    {
        public OrderHandlerWaitingForOnePlayerState(OrderHandler orderHandler, GameManager manager)
            : base(orderHandler, manager)
        {

        }
        public override void HandleOrder(Order order)
        {
            if (order.Player == 1)
            {
                orderHandler.Player1Order = order;
                manager.FreezePlayer1UI();
            }
            else
            {
                orderHandler.Player2Order = order;
                manager.FreezePlayer2UI();
            }
            orderHandler.ExecuteOrders();
        }
    }

}
