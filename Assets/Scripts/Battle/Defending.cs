﻿using UnityEngine;
using System.Collections;

namespace ARCardGame.BattleSystem
{
    public class Defending : MonoBehaviour, IDefending
    {
        private bool defending = false;

        public bool DefenseState
        {
            get { return defending; }
            set { defending = value; }
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
