﻿namespace ARCardGame.BattleSystem
{
    public interface IDefending
    {
        bool DefenseState { get; set; }
    }
}
