﻿using UnityEngine;
using System.Collections;
using ARCardGame.DetectionSystem;

namespace ARCardGame.Managers
{
    public delegate void TargetsAreReadyEvent(object sender, TargetsAreReadyEventArgs eventArgs);
    #region Interfaces
    public interface ITargetManager
    {
        GameObject Player1Monster { get; set; }
        GameObject Player2Monster { get; set; }

        void ObjectDetected(object sender, DetectionEventArgs eventArgs);
        void ObjectLost(object sender, DetectionEventArgs eventArgs);

        event TargetsAreReadyEvent GameReady;
    }

    public interface ITargetManagerStateChange
    {
        void ChangeStateToReady();
        void ChangeStateToWaitingForBoth();
        void ChangeStateToWaitingForPlayer2();
    }
    #endregion

    #region MonoBehaviorClass
    public class TargetManager : MonoBehaviour, ITargetManager
    {

        public GameObject Player1Monster
        {
            get
            {
                if (targetManagerController == null)
                    return null;
                return targetManagerController.Player1Monster;
            }
            set
            {
                if (targetManagerController == null)
                    targetManagerController = new TargetManagerController();
                targetManagerController.Player1Monster = value;
            }
        }
        public GameObject Player2Monster
        {
            get
            {
                if (targetManagerController == null)
                    return null;
                return targetManagerController.Player2Monster;
            }
            set
            {
                if (targetManagerController == null)
                    targetManagerController = new TargetManagerController();
                targetManagerController.Player2Monster = value;
            }
        }

        private ITargetManager targetManagerController;

        void Start()
        {
            if (targetManagerController == null)
                targetManagerController = new TargetManagerController();
        }

        public void ObjectDetected(object sender, DetectionEventArgs eventArgs)
        {
            targetManagerController.ObjectDetected(sender, eventArgs);
        }
        public void ObjectLost(object sender, DetectionEventArgs eventArgs)
        {
            targetManagerController.ObjectLost(sender, eventArgs);
        }


        private void GameEndedHandler()
        {
            //TODO Later.
        }
        #region EventTriggers

        public event TargetsAreReadyEvent GameReady
        {
            add
            {
                if (targetManagerController == null)
                    targetManagerController = new TargetManagerController();
                targetManagerController.GameReady += value;
            }
            remove
            {
                if (targetManagerController != null)
                    targetManagerController.GameReady -= value;
            }
        }
        #endregion
    }
    #endregion

    #region UnityAPIFreeClass

    public class TargetManagerController : ITargetManager, ITargetManagerStateChange
    {


        public GameObject Player1Monster
        {
            get { return player1Monster; }
            set { player1Monster = value; }
        }
        public GameObject Player2Monster
        {
            get { return player2Monster; }
            set { player2Monster = value; }
        }

        private GameObject player2Monster;
        private GameObject player1Monster;

        private TargetManagerState state;
        private TargetManagerState waitingForBoth;
        private TargetManagerState waitingForPlayer2;
        private TargetManagerState ready;

        public TargetManagerController()
        {
            waitingForBoth = new TargetManagerWaitingForBothPlayers(this, this);
            waitingForPlayer2 = new TargetManagerWaitingForSecondPlayer(this, this);
            ready = new TargetManagerReady(this, this);
            state = waitingForBoth;
        }
        public void ObjectDetected(object sender, DetectionEventArgs eventArgs)
        {
            state.ObjectDetected(sender, eventArgs);
        }
        public void ObjectLost(object sender, DetectionEventArgs eventArgs)
        {
            state.ObjectLost(sender, eventArgs);
        }

        public void ChangeStateToWaitingForBoth()
        {
            state = waitingForBoth;
        }
        public void ChangeStateToWaitingForPlayer2()
        {
            state = waitingForPlayer2;
        }
        public void ChangeStateToReady()
        {
            OnGameReady();
            state = ready;
        }

        private void GameEndedHandler()
        {
            //TODO Later.
        }
        #region EventTriggers

        public event TargetsAreReadyEvent GameReady;
        private void OnGameReady()
        {
            if (GameReady != null) // will be null if no subscribers
            {
                GameReady(this, new TargetsAreReadyEventArgs { Player1Monster = this.player1Monster, Player2Monster = this.player2Monster });
            }
        }
        #endregion
    }
    #endregion

    #region StateClasses
    abstract class TargetManagerState
    {
        protected ITargetManager targetManager;
        protected ITargetManagerStateChange targetManagerStateChanger;
        public TargetManagerState(ITargetManager targetManager, ITargetManagerStateChange targetManagerStateChanger)
        {
            this.targetManager = targetManager;
            this.targetManagerStateChanger = targetManagerStateChanger;
        }
        public virtual void ObjectDetected(object sender, DetectionEventArgs eventArgs)
        {

        }
        public virtual void ObjectLost(object sender, DetectionEventArgs eventArgs)
        {

        }
    }

    class TargetManagerWaitingForBothPlayers : TargetManagerState
    {


        public TargetManagerWaitingForBothPlayers(ITargetManager targetManager, ITargetManagerStateChange targetManagerStateChanger)
            : base(targetManager, targetManagerStateChanger)
        {

        }
        public override void ObjectDetected(object sender, DetectionEventArgs eventArgs)
        {
            targetManager.Player1Monster = eventArgs.GameObject;
            targetManagerStateChanger.ChangeStateToWaitingForPlayer2();
        }
        public override void ObjectLost(object sender, DetectionEventArgs eventArgs)
        {
            //Do Nothing.
        }
    }

    class TargetManagerWaitingForSecondPlayer : TargetManagerState
    {
        public TargetManagerWaitingForSecondPlayer(ITargetManager targetManager, ITargetManagerStateChange targetManagerStateChanger)
            : base(targetManager, targetManagerStateChanger)
        {

        }
        public override void ObjectDetected(object sender, DetectionEventArgs eventArgs)
        {
            targetManager.Player2Monster = eventArgs.GameObject;
            targetManagerStateChanger.ChangeStateToReady();

        }
        public override void ObjectLost(object sender, DetectionEventArgs eventArgs)
        {
            targetManager.Player1Monster = null;
            targetManagerStateChanger.ChangeStateToWaitingForBoth();
        }
    }

    class TargetManagerReady : TargetManagerState
    {
        public TargetManagerReady(ITargetManager targetManager, ITargetManagerStateChange targetManagerStateChanger)
            : base(targetManager, targetManagerStateChanger)
        {
        }
    }
    #endregion
}
