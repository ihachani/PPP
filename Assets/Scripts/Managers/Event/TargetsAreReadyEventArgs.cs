﻿using System;
using UnityEngine;

namespace ARCardGame.Managers
{
    public class TargetsAreReadyEventArgs : EventArgs
    {
        GameObject player1Monster;

        public GameObject Player1Monster
        {
            get { return player1Monster; }
            set { player1Monster = value; }
        }

        GameObject player2Monster;

        public GameObject Player2Monster
        {
            get { return player2Monster; }
            set { player2Monster = value; }
        }
        public TargetsAreReadyEventArgs()
        {

        }
        public TargetsAreReadyEventArgs(GameObject player1Monster, GameObject player2Monster)
        {
            this.player1Monster = player1Monster;
            this.player2Monster = player2Monster;
        }
    }
}