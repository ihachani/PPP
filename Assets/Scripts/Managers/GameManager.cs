﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ARCardGame.BattleSystem;
using ARCardGame.Models.Meters;
using ARCardGame.Models.MonstersStatus;
using ARCardGame.Models.Orders;
using ARCardGame.UIControllers;
using ARCardGame.AnimationSystem;

namespace ARCardGame.Managers
{
    public class GameManager : MonoBehaviour
    {
        public AssociateUIWithCharcter player1Associator;
        public AssociateUIWithCharcter player2Associator;
        public OrderHandler orderHandler;
        public UIFreezeUnfreezeController Player1UIFreezeUnfreezeController;
        public UIFreezeUnfreezeController Player2UIFreezeUnfreezeController;
        // Use this for initialization
        void Start()
        {
            GetComponent<TargetManager>().GameReady += GameManager_GameReady;
        }


        // Update is called once per frame
        void Update()
        {

        }

        public void HandleOder(Order order)
        {
            orderHandler.HandleOrder(order);
        }
        void GameManager_GameReady(object sender, TargetsAreReadyEventArgs eventArgs)
        {
            player1Associator.AssociateUI(GetFirstChild(eventArgs.Player1Monster));
            player2Associator.AssociateUI(GetFirstChild(eventArgs.Player2Monster));
        }

        private GameObject GetFirstChild(GameObject gameObject)
        {
            // TODO Make this in a helper class.
            return gameObject.transform.GetChild(0).gameObject;
        }

        public void HandleBattle(List<OrderResult> results, List<Order> orders)
        {
            var player1Status = orders[0].Charcter.GetComponent<MonsterStatus>();
            var player2Status = orders[1].Charcter.GetComponent<MonsterStatus>();

            var player1Animator = orders[0].Charcter.GetComponent<Animator>();
            var player2Animator = orders[1].Charcter.GetComponent<Animator>();

            if (player1Status.Speed > player2Status.Speed)
            {
                StartCoroutine(Player1BeforePlayer2(results, orders));

            }
            else if (player1Status.Speed < player2Status.Speed)
            {
                StartCoroutine(Player2BeforePlayer1(results, orders));

            }
            else
            {
                System.Random rand = new System.Random();

                if (rand.Next(0, 2) == 0)
                {
                    StartCoroutine(Player1BeforePlayer2(results, orders));
                }
                else
                {
                    StartCoroutine(Player2BeforePlayer1(results, orders));
                }
            }
            BlockUnusableAttacks(orders);
            UnfreezeUI();
        }

        private static IEnumerator Player2BeforePlayer1(List<OrderResult> results, List<Order> orders)
        {
            var player1Animator = orders[0].Charcter.GetComponent<Animator>();
            var player2Animator = orders[1].Charcter.GetComponent<Animator>();

            TriggerAttackAnimation(orders[1]);

            yield return null;
            while (player2Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);

            DecreaseMana(results[1], orders[1]);
            DecreaseStamina(results[1], orders[1]);

            TriggerHitAnimation(orders[0]);
            yield return new WaitForSeconds(0.5f);

            yield return null;
            while (player1Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {

                yield return null;
            }
            yield return new WaitForSeconds(0.5f);

            InflictDamage(results[1], orders[0]);

            TriggerAttackAnimation(orders[0]);
            yield return null;
            while (player1Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);

            DecreaseMana(results[0], orders[0]);
            DecreaseStamina(results[0], orders[0]);

            TriggerHitAnimation(orders[1]);
            yield return new WaitForSeconds(0.5f);

            yield return null;
            while (player2Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);

            InflictDamage(results[0], orders[1]);
            yield return new WaitForSeconds(0.5f);
        }

        private static IEnumerator Player1BeforePlayer2(List<OrderResult> results, List<Order> orders)
        {
            var player1Animator = orders[0].Charcter.GetComponent<Animator>();
            var player2Animator = orders[1].Charcter.GetComponent<Animator>();
            TriggerAttackAnimation(orders[0]);
            yield return null;
            while (player1Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);

            DecreaseMana(results[0], orders[0]);
            DecreaseStamina(results[0], orders[0]);

            TriggerHitAnimation(orders[1]);
            yield return new WaitForSeconds(0.5f);

            yield return null;
            while (player2Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(1f);

            InflictDamage(results[0], orders[1]);
            yield return new WaitForSeconds(0.5f);
            TriggerAttackAnimation(orders[1]);

            yield return null;
            while (player2Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);

            DecreaseMana(results[1], orders[1]);
            DecreaseStamina(results[1], orders[1]);

            TriggerHitAnimation(orders[0]);
            yield return new WaitForSeconds(0.5f);

            yield return null;
            while (player1Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {

                yield return null;
            }
            yield return new WaitForSeconds(1f);

            InflictDamage(results[1], orders[0]);
        }

        private static void InflictDamage(OrderResult result, Order order)
        {
            GetMetersListFromOrder(order).HealthMeter.Decrease(result.TotalDamageDealt);
        }

        private static MetersList GetMetersListFromOrder(Order order)
        {
            return order.Charcter.GetComponent<MetersList>();
        }

        private static void TriggerHitAnimation(Order order)
        {
            order.Charcter.GetComponent<ChacterAnimatorController>().SetTriggerHit();
        }

        private static void DecreaseStamina(OrderResult result, Order order)
        {
            GetMetersListFromOrder(order).StaminaMeter.Decrease(result.TotalStaminaLost);
        }

        private static void DecreaseMana(OrderResult result, Order order)
        {
            GetMetersListFromOrder(order).ManaMeter.Decrease(result.TotalStaminaLost);
        }

        private static void TriggerAttackAnimation(Order order)
        {
            order.Charcter.GetComponent<ChacterAnimatorController>().TriggerAttackAnimation(order.Attack.AnimationName);
        }


        public IEnumerator PlayOnce(Animator changedAnimator)
        {
            yield return null;
            while (changedAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
            {
                yield return null;
            }
        }
        //private IEnumerator WaitForAnimation(Animation animation)
        //{
        //    do
        //    {
        //        yield return null;
        //    } while (animation.isPlaying);
        //}

        //public IEnumerator PlayOnce(Animator changedAnimator, string paramName)
        //{
        //    changedAnimator.SetBool(paramName, true);
        //    yield return null;
        //    changedAnimator.SetBool(paramName, false);
        //    while (PlayerController.currentAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
        //    {
        //        yield return null;
        //    }
        //}

        private void UnfreezeUI()
        {
            Player1UIFreezeUnfreezeController.UnFreezeUI();
            Player2UIFreezeUnfreezeController.UnFreezeUI();
        }

        private void BlockUnusableAttacks(List<Order> orders)
        {
            Player1UIFreezeUnfreezeController.BlockUnusableAttacks(orders[0].Charcter);
            Player2UIFreezeUnfreezeController.BlockUnusableAttacks(orders[1].Charcter);
        }

        public void FreezePlayer1UI()
        {
            Player1UIFreezeUnfreezeController.FreezeUI();
        }
        public void FreezePlayer2UI()
        {
            Player2UIFreezeUnfreezeController.FreezeUI();
        }
    }

}
