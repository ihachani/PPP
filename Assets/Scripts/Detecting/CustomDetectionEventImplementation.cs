﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARCardGame.DetectionSystem
{
    [Serializable]
    public class CustomDetectionEventImplementation : ICustomDetectionEventTrigger
    {

        private GameObject gameObject;
        public GameObject GameObject
        {
            set { gameObject = value; }
        }


        public void OnTrackableStateChanged(Vuforia.TrackableBehaviour.Status previousStatus, Vuforia.TrackableBehaviour.Status newStatus)
        {
            {
                if (newStatus == Vuforia.TrackableBehaviour.Status.DETECTED ||
                    newStatus == Vuforia.TrackableBehaviour.Status.TRACKED ||
                    newStatus == Vuforia.TrackableBehaviour.Status.EXTENDED_TRACKED)
                {
                    OnTrackingFound();
                }
                else
                {
                    OnTrackingLost();
                }
            }
        }
        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            OnObjectDetect();
        }


        private void OnTrackingLost()
        {
            OnObjectLost();
        }

        #endregion // PRIVATE_METHODS
        #region EventTriggers

        public event DetectionStatusChanged ObjectDetect;
        private void OnObjectDetect()
        {
            if (ObjectDetect != null) // will be null if no subscribers
            {
                ObjectDetect(this, new DetectionEventArgs { GameObject = this.gameObject, DetectionStatus = true });
            }
        }
        public event DetectionStatusChanged ObjectLost;
        private void OnObjectLost()
        {
            if (ObjectLost != null) // will be null if no subscribers
            {
                ObjectLost(this, new DetectionEventArgs { GameObject = this.gameObject, DetectionStatus = false });
            }
        }
        #endregion
    }


}
