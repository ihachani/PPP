﻿using UnityEngine;
using System.Collections;
using Vuforia;
using ARCardGame.Managers;
namespace ARCardGame.DetectionSystem
{
    public class CustomEventTrigger : MonoBehaviour, ITrackableEventHandler
    {
        public TargetManager targetManager;
        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;
        private ICustomDetectionEventTrigger customEventTrigger;
        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS

        void Start()
        {
            customEventTrigger = new CustomDetectionEventImplementation();
            customEventTrigger.GameObject = this.gameObject;
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
            customEventTrigger.ObjectDetect += targetManager.ObjectDetected;
            customEventTrigger.ObjectLost += targetManager.ObjectLost;
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            customEventTrigger.OnTrackableStateChanged(previousStatus, newStatus);
        }

        #endregion // PUBLIC_METHODS




    } 
}