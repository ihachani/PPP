﻿using System;
using UnityEngine;

namespace ARCardGame.DetectionSystem
{
    public delegate void DetectionStatusChanged(object sender, DetectionEventArgs eventArgs);


    public interface ICustomDetectionEventTrigger
    {
        GameObject GameObject { set; }

        event DetectionStatusChanged ObjectDetect;
        event DetectionStatusChanged ObjectLost;
        void OnTrackableStateChanged(global::Vuforia.TrackableBehaviour.Status previousStatus, global::Vuforia.TrackableBehaviour.Status newStatus);
    } 
}

