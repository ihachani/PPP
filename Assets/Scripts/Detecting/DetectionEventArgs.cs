﻿using System;
using UnityEngine;

namespace ARCardGame.DetectionSystem
{
    public class DetectionEventArgs : EventArgs
    {
        GameObject gameObject;

        public GameObject GameObject
        {
            get { return gameObject; }
            set { gameObject = value; }
        }
        bool detectionStatus;

        public bool DetectionStatus
        {
            get { return detectionStatus; }
            set { detectionStatus = value; }
        }

        public DetectionEventArgs()
        {

        }
        public DetectionEventArgs(GameObject gameObject, bool detectionStatus)
        {
            this.gameObject = gameObject;
            this.detectionStatus = detectionStatus;
        }
    }

    
}