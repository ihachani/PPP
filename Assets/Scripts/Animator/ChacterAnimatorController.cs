﻿using UnityEngine;
using System.Collections;

namespace ARCardGame.AnimationSystem
{
    public class ChacterAnimatorController : MonoBehaviour
    {
        const string ATTACK1 = "Attack1";
        const string ATTACK2 = "Attack2";
        const string ATTACK3 = "Attack3";
        const string CASTMAGIC = "CastMagic";
        const string DEFEND = "Defend";
        const string TAKEDAMAGE = "TakeDamage";
        Animator animatorController;
        void Start()
        {
            animatorController = GetComponent<Animator>();
        }
        public void SetTriggerHit()
        {
            animatorController.SetTrigger(TAKEDAMAGE);
        }

        public void SetAttack1Trigger()
        {
            animatorController.SetTrigger(ATTACK1);
        }
        public void SetAttack2Trigger()
        {
            animatorController.SetTrigger(ATTACK2);
        }
        public void SetAttack3Trigger()
        {
            animatorController.SetTrigger(ATTACK3);
        }
        public void SetCastMagicTrigger()
        {
            animatorController.SetTrigger(CASTMAGIC);
        }
        public void SetDefendBoolean(bool value)
        {
            animatorController.SetBool(DEFEND, value);
        }
        public void ToggleDefend()
        {
            animatorController.SetBool(DEFEND, !animatorController.GetBool(DEFEND));
        }
        public void TriggerAttackAnimation(string name)
        {
            animatorController.SetTrigger(name);
        }

    }
}

