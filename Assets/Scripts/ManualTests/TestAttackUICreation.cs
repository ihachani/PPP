﻿using UnityEngine;
using System.Collections;
using ARCardGame.Models.Attacks;
using ARCardGame.UIControllers;

namespace ARCardGame.ManualTests
{
    public class TestAttackUICreation : MonoBehaviour
    {
        public FillAttackUIInfo fillAttackUIInfo;
        public AttackModel attackModel;
        public GameObject parentPanel;
        // Use this for initialization
        void Start()
        {
            GameObject newUIElement = fillAttackUIInfo.FillAttackUI(attackModel);
            newUIElement.transform.SetParent(parentPanel.transform, false);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
    
}