﻿using UnityEngine;
using System.Collections;
using ARCardGame.Models.Attacks;
using ARCardGame.UIControllers;

namespace ARCardGame.ManualTests
{
    public class TestAttackListGenerator : MonoBehaviour
    {
        public AttacksListUICreator listcreator;
        public AttackModel[] attacksList;
        public GameObject charcter;
        // Use this for initialization
        void Start()
        {
            listcreator.GenerateList(charcter);
            //listcreator.generateList(attacksList);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
    
}