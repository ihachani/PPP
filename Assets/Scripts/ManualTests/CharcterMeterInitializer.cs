﻿using UnityEngine;
using System.Collections;
using ARCardGame.Models.Meters;
using ARCardGame.UIControllers;

namespace ARCardGame.ManualTests
{
    public class CharcterMeterInitializer : MonoBehaviour
    {
        public MeterController meterController;
        public Meter meter;
        // Use this for initialization
        void Start()
        {
            meterController.InitializeMeter(meter);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
    
}