﻿using System;
using UnityEngine;
namespace ARCardGame.Models.Orders
{
    public interface IOrder
    {
        Animator Animator { get; set; }
        ARCardGame.Models.Attacks.IAttackModel Attack { get; set; }
        ARCardGame.AnimationSystem.ChacterAnimatorController ChacterAnimatorController { get; set; }
        GameObject Charcter { get; set; }
        ARCardGame.Models.MonstersStatus.IMonsterStatus MonsterStatus { get; set; }
        bool DefenseState { get; set; }
        int Player { get; set; }
    }
}
