﻿using ARCardGame.Models.Attacks;
using ARCardGame.Models.MonstersStatus;
using UnityEngine;

namespace ARCardGame.Models.Orders
{
    public class Order : IOrder
    {

        GameObject charcter;

        public GameObject Charcter
        {
            get { return charcter; }
            set { charcter = value; }
        }

        IMonsterStatus monsterStatus;

        public IMonsterStatus MonsterStatus
        {
            get { return monsterStatus; }
            set { monsterStatus = value; }
        }

        bool defenseState;

        public bool DefenseState
        {
            get { return defenseState; }
            set { defenseState = value; }
        }

        Animator animator;

        public Animator Animator
        {
            get { return animator; }
            set { animator = value; }
        }

        ARCardGame.AnimationSystem.ChacterAnimatorController chacterAnimatorController;

        public ARCardGame.AnimationSystem.ChacterAnimatorController ChacterAnimatorController
        {
            get { return chacterAnimatorController; }
            set { chacterAnimatorController = value; }
        }

        IAttackModel attack;
        private AttackModel charcter1;

        public IAttackModel Attack
        {
            get { return attack; }
            set { attack = value; }
        }
        int player;

        public int Player
        {
            get { return player; }
            set { player = value; }
        }

        public Order()
        {

        }

        public Order(AttackModel attack, GameObject charcter, int player)
        {
            this.attack = attack;
            this.charcter = charcter;
            this.player = player;
        }

    }

}
