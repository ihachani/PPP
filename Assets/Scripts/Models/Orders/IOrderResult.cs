﻿using System;
namespace ARCardGame.Models.Orders
{
    public interface IOrderResult
    {
        int TotalDamageDealt { get; set; }
        int TotalDamageRecieved { get; set; }
        int TotalHealingRecieved { get; set; }
        int TotalManaLost { get; set; }
        int TotalManaRecieved { get; set; }
        int TotalStaminaLost { get; set; }
        int TotalStaminaRecieved { get; set; }
    }
}
