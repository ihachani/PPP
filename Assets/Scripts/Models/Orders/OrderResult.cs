﻿using System;


namespace ARCardGame.Models.Orders
{
    public class OrderResult : IOrderResult
    {
        int totalDamageDealt;

        public int TotalDamageDealt
        {
            get { return totalDamageDealt; }
            set { totalDamageDealt = value; }
        }
        int totalDamageRecieved;

        public int TotalDamageRecieved
        {
            get { return totalDamageRecieved; }
            set { totalDamageRecieved = value; }
        }
        int totalHealingRecieved;

        public int TotalHealingRecieved
        {
            get { return totalHealingRecieved; }
            set { totalHealingRecieved = value; }
        }
        int totalManaLost;

        public int TotalManaLost
        {
            get { return totalManaLost; }
            set { totalManaLost = value; }
        }
        int totalManaRecieved;

        public int TotalManaRecieved
        {
            get { return totalManaRecieved; }
            set { totalManaRecieved = value; }
        }
        int totalStaminaLost;

        public int TotalStaminaLost
        {
            get { return totalStaminaLost; }
            set { totalStaminaLost = value; }
        }
        int totalStaminaRecieved;

        public int TotalStaminaRecieved
        {
            get { return totalStaminaRecieved; }
            set { totalStaminaRecieved = value; }
        }

    }

    
}