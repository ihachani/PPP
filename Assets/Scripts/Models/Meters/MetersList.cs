﻿using UnityEngine;
using System.Collections;
using ARCardGame.Models.MonstersStatus;
/// <summary>
/// MonoBehaviour class that conatins a charcter Meters.
/// Used to allow testing.
/// </summary>
namespace ARCardGame.Models.Meters
{
    public class MetersList : MonoBehaviour
    {
        [SerializeField]
        private Meter healthMeter;

        public Meter HealthMeter
        {
            get { return healthMeter; }
        }
        [SerializeField]
        private Meter manaMeter;

        public Meter ManaMeter
        {
            get { return manaMeter; }
        }
        [SerializeField]
        private Meter staminaMeter;

        public Meter StaminaMeter
        {
            get { return staminaMeter; }
        }
        void Awake()
        {
            InitializeMetersList();
        }
        private void InitializeMetersList()
        {
            MonsterStatus status = GetComponent<MonsterStatus>();
            healthMeter = InitializeMeter("Health", status.MaxHealth);
            manaMeter = InitializeMeter("Health", status.MaxMana);
            staminaMeter = InitializeMeter("Health", status.MaxStamina);
        }
        private Meter InitializeMeter(string name, int maxValue)
        {
            Meter meter = new Meter(name, maxValue, this.gameObject);
            return meter;
        }

    }
    
}