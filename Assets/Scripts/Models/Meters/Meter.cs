﻿using UnityEngine;
using System.Collections;
using System;

namespace ARCardGame.Models.Meters
{
    [Serializable]
    public class Meter : IMeter
    {
        private GameObject gameObject;

        public GameObject GameObject
        {
            set { gameObject = value; }
        }
        [SerializeField]
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        [SerializeField]
        private int maxValue;

        public int MaxValue
        {
            get { return maxValue; }
            set { maxValue = value; }
        }
        [SerializeField]
        private int currentValue;

        public int CurrentValue
        {
            get { return currentValue; }
            set
            {
                SetValue(value);
            }
        }

        public Meter(string name, int maxValue, GameObject gameObject)
        {
            this.name = name;
            this.maxValue = maxValue;
            this.currentValue = maxValue;
            this.gameObject = gameObject;
        }
        /// <summary>
        /// Increase currentHealth by value
        /// </summary>
        /// <param name="value">Value to Increase Health with</param>
        public void increase(int value)
        {

            if (value > 0)
            {
                if (CheckMaxHealthOverBord(value))
                {
                    currentValue += value;
                    OnValueIncrease();
                }
                else
                {
                    currentValue = maxValue;
                    OnValueIncrease();
                }
            }
        }
        private bool CheckMaxHealthOverBord(int value)
        {
            return (currentValue + value) < maxValue;
        }
        public void Decrease(int value)
        {

            if (value > 0)
            {
                if ((currentValue - value) > 0)
                {
                    currentValue -= value;
                    OnValueDecrease();
                }
                else
                {
                    currentValue = 0;
                    OnValueDecrease();
                    OnValueIs0();
                }
            }
        }

        public void SetValue(int value)
        {
            if (value > maxValue)
            {
                value = maxValue;

            }
            else if (value < 0)
            {
                value = 0;
            }
            if (currentValue < value)
            {
                currentValue = value;
                OnValueIncrease();
            }
            else
            {
                currentValue = value;
                OnValueDecrease();
                if (value == 0)
                    OnValueIs0();
            }
        }

        #region Events

        //public delegate void ValueChanged(object sender, MeterEventArgs eventArgs);
        //public delegate void ObjectDead(object sender, DeadEventArgs eventArgs);
        public event MeterValueChanged ValueIncrease;
        private void OnValueIncrease()
        {
            if (ValueIncrease != null) // will be null if no subscribers
            {
                ValueIncrease(this, new MeterEventArgs { CurrentValue = currentValue, MaxValue = maxValue });
            }
        }


        public event MeterValueChanged ValueDecrease;
        private void OnValueDecrease()
        {
            if (ValueDecrease != null) // will be null if no subscribers
            {
                ValueDecrease(this, new MeterEventArgs { CurrentValue = currentValue, MaxValue = maxValue });
            }
        }
        public event MeterValueIS0 ValueIs0;
        private void OnValueIs0()
        {
            if (ValueIs0 != null) // will be null if no subscribers
            {
                ValueIs0(this, new DeadEventArgs(this.gameObject));
            }
        }
        #endregion


    }
    #region EventArgsClass
    public class MeterEventArgs : EventArgs
    {
        int maxValue;

        public int MaxValue
        {
            get { return maxValue; }
            set { maxValue = value; }
        }
        int currentValue;

        public int CurrentValue
        {
            get { return currentValue; }
            set { currentValue = value; }
        }
        public MeterEventArgs(int maxValue, int currentValue)
        {
            this.maxValue = maxValue;
            this.currentValue = currentValue;
        }

        public MeterEventArgs()
        {
        }

    }
    public class DeadEventArgs : EventArgs
    {
        GameObject deadObject;

        public GameObject DeadObject
        {
            get { return deadObject; }
            set { deadObject = value; }
        }

        public DeadEventArgs()
        {

        }
        public DeadEventArgs(GameObject deadObject)
        {
            this.deadObject = deadObject;
        }
    }
    #endregion
}
