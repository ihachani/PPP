﻿using System;

namespace ARCardGame.Models.Meters
{
    public delegate void MeterValueChanged(object sender, MeterEventArgs eventArgs);
    public delegate void MeterValueIS0(object sender, DeadEventArgs eventArgs);
    public interface IMeter
    {
        void Decrease(int value);
        void increase(int value);

        string Name { get; set; }
        int CurrentValue { get; set; }
        int MaxValue { get; set; }
        UnityEngine.GameObject GameObject { set; }

        event MeterValueChanged ValueDecrease;
        event MeterValueChanged ValueIncrease;
        event MeterValueIS0 ValueIs0;
    }

    
}