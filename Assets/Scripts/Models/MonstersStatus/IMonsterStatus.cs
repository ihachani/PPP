﻿namespace ARCardGame.Models.MonstersStatus
{
    public interface IMonsterStatus
    {
        int PhysicalAttack { get; set; }
        int PhysicalDefense { get; set; }
        int MagicAttack { get; set; }
        int MagicDefense { get; set; }
        int MaxHealth { get; set; }
        int MaxMana { get; set; }
        int MaxStamina { get; set; }
        string Name { get; set; }
        int Speed { get; set; }
    }
}
