﻿using UnityEngine;
using System.Collections;
using ARCardGame.Models.MonstersStatus;

namespace ARCardGame.Models.MonstersStatus
{
    public class MonsterStatus : MonoBehaviour, IMonsterStatus
    {

        [SerializeField]
        private BaseMonsterStatus baseMonsterStatus;

        [SerializeField]
        new private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [SerializeField]
        private int maxHealth;
        public int MaxHealth
        {
            get { return maxHealth; }
            set { maxHealth = value; }
        }

        [SerializeField]
        private int maxMana;
        public int MaxMana
        {
            get { return maxMana; }
            set { maxMana = value; }
        }

        [SerializeField]
        private int maxStamina;
        public int MaxStamina
        {
            get { return maxStamina; }
            set { maxStamina = value; }
        }

        [SerializeField]
        private int attack;
        public int PhysicalAttack
        {
            get { return attack; }
            set { attack = value; }
        }

        [SerializeField]
        private int defense;
        public int PhysicalDefense
        {
            get { return defense; }
            set { defense = value; }
        }

        [SerializeField]
        private int magicAttack;
        public int MagicAttack
        {
            get { return magicAttack; }
            set { magicAttack = value; }
        }

        [SerializeField]
        private int magicDefense;
        public int MagicDefense
        {
            get { return magicDefense; }
            set { magicDefense = value; }
        }

        [SerializeField]
        private int speed;
        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }
    }
    
}