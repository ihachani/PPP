﻿using System;
namespace ARCardGame.Models.Attacks
{
    public interface IAttackModel
    {
        string AnimationName { get; set; }
        int MagicDamage { get; set; }
        int ManaPrice { get; set; }
        string Name { get; set; }
        int PhysicalDamage { get; set; }
        int StaminaPrice { get; set; }
    }
}
