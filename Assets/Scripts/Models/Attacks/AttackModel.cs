﻿using System;
using UnityEngine;
namespace ARCardGame.Models.Attacks
{
    [Serializable]
    public class AttackModel : IAttackModel
    {
        [SerializeField]
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [SerializeField]
        private int physicalDamage;
        public int PhysicalDamage
        {
            get { return physicalDamage; }
            set { physicalDamage = value; }
        }

        [SerializeField]
        private int magicDamage;
        public int MagicDamage
        {
            get { return magicDamage; }
            set { magicDamage = value; }
        }

        [SerializeField]
        private int manaPrice;
        public int ManaPrice
        {
            get { return manaPrice; }
            set { manaPrice = value; }
        }

        [SerializeField]
        private int staminaPrice;
        public int StaminaPrice
        {
            get { return staminaPrice; }
            set { staminaPrice = value; }
        }

        [SerializeField]
        private string animationName;
        public string AnimationName
        {
            get { return animationName; }
            set { animationName = value; }
        }
    }

}
