﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ARCardGame.Models.Attacks;
using ARCardGame.Models.Meters;

namespace ARCardGame.UIControllers
{
    public class UIFreezeUnfreezeController : MonoBehaviour
    {
        List<GameObject> disabled = new List<GameObject>();
        public GameObject attackListPanelUI;
        public AttackMenuController playerAttackMenu;
        public void BlockUnusableAttacks(GameObject charcter)
        {
            var attackList = charcter.GetComponent<AttacksList>().AttackList;
            var manaMeter = GetCharcterGameObjectManaMeter(charcter);
            var staminaMeter = GetCharcterGameObjectStaminaMeter(charcter);
            int i = 0;
            foreach (var attack in attackList)
            {
                if (CheckAttackUnusable(manaMeter, staminaMeter, attack))
                {
                    DisableAttackUIPanel(i);
                }
                i++;
            }
        }

        private static Meter GetCharcterGameObjectStaminaMeter(GameObject charcter)
        {
            return charcter.GetComponent<MetersList>().StaminaMeter;
        }

        private static Meter GetCharcterGameObjectManaMeter(GameObject charcter)
        {
            return charcter.GetComponent<MetersList>().ManaMeter;
        }

        private void DisableAttackUIPanel(int i)
        {
            Debug.Log("Index i: ", this);
            GameObject uiAttack = GetGameObjectChild(i);
            Debug.Log("uiAttack Name: " + uiAttack.name, this);
            var canvasGroup = GetCanvasGroup(uiAttack);
            DisableCanvaseGroup(canvasGroup);
            disabled.Add(uiAttack);
        }

        private static void DisableCanvaseGroup(CanvasGroup canvasGroup)
        {
            canvasGroup.alpha = 0.5f;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        private static CanvasGroup GetCanvasGroup(GameObject uiAttack)
        {
            var canvasGroup = uiAttack.GetComponent<CanvasGroup>();
            return canvasGroup;
        }

        private GameObject GetGameObjectChild(int i)
        {
            //AnAttack UI is first child temporary that is the reason behind the offset.
            GameObject uiAttack = attackListPanelUI.transform.GetChild(i + 1).gameObject;
            return uiAttack;
        }

        private static bool CheckAttackUnusable(Meter manaMeter, Meter staminaMeter, AttackModel attack)
        {
            return attack.ManaPrice > manaMeter.CurrentValue || attack.StaminaPrice > staminaMeter.CurrentValue;
        }

        public void FreezeUI()
        {
            playerAttackMenu.HideBoth();
        }
        public void UnFreezeUI()
        {
            playerAttackMenu.HideAttacksList();
        }
    }
    
}