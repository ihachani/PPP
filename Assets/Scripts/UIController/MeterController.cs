﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ARCardGame.Models.Meters;

namespace ARCardGame.UIControllers
{
    [RequireComponent(typeof(Slider))]
    public class MeterController : MonoBehaviour
    {
        Slider meter;
        // Use this for initialization
        void Start()
        {
            meter = GetComponent<Slider>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void InitializeMeter(IMeter meter)
        {
            InitializeUI(meter);
            InitializeEvents(meter);
        }
        private void InitializeUI(IMeter meter)
        {
            this.meter.maxValue = meter.MaxValue;
            this.meter.value = meter.CurrentValue;
        }
        private void InitializeEvents(IMeter meter)
        {
            meter.ValueIncrease += Meter_ValueChanged;
            meter.ValueDecrease += Meter_ValueChanged;
        }

        private void Meter_ValueChanged(object sender, MeterEventArgs eventArgs)
        {
            meter.value = eventArgs.CurrentValue;
        }
    }

}