﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ARCardGame.Managers;
using ARCardGame.Models.Attacks;
using ARCardGame.Models.Orders;

namespace ARCardGame.UIControllers
{
    [RequireComponent(typeof(RectTransform))]
    public class AttacksListUICreator : MonoBehaviour
    {
        [SerializeField]
        private FillAttackUIInfo fillAttackUIInfo;
        [SerializeField]
        private int player;
        private RectTransform parentTransform;
        private GameManager manager;
        
        void Awake()
        {
            parentTransform = GetComponent<RectTransform>();
            manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        }
        public void GenerateList(GameObject charcter)
        {
            generateList(charcter.GetComponent<AttacksList>().AttackList, charcter);
        }
        private void generateList(AttackModel[] attacksList, GameObject charcter)
        {
            //TODO REFACTOR THIS.
            // Maybe merge this method.
            int i = 0;
            foreach (var attack in attacksList)
            {
                var tempUI = fillAttackUIInfo.FillAttackUI(attack);
                SetTransformParent(tempUI);
                RectTransform tempRectTransform = tempUI.GetComponent<RectTransform>();
                SetParentHeight(tempRectTransform);
                SetNewElementPosition(i, tempRectTransform);
                AddListnerToUIElement(attack, tempUI, charcter);
                i++;
            }
        }



        private void SetParentHeight(RectTransform tempRectTransform)
        {
            parentTransform.sizeDelta = parentTransform.sizeDelta + new Vector2(0, tempRectTransform.sizeDelta.y);
        }

        private void SetNewElementPosition(int i, RectTransform tempRectTransform)
        {
            tempRectTransform.localPosition = new Vector3(0, -1 * (i * tempRectTransform.sizeDelta.y), 0);
        }

        private void SetTransformParent(GameObject tempUI)
        {
            tempUI.transform.SetParent(parentTransform.transform, false);
        }
        private void AddListnerToUIElement(AttackModel attack, GameObject tempUI, GameObject charcter)
        {
            Button button = tempUI.GetComponent<Button>();
            AddListenerToButton(button, attack, charcter);
        }
        void AddListenerToButton(Button button, AttackModel attack, GameObject charcter)
        {
            button.onClick.AddListener(() => btnClicked(attack, charcter));
        }
        public void btnClicked(AttackModel attack, GameObject charcter)
        {
            Order order = new Order(attack: attack, charcter: charcter, player: player);
            manager.HandleOder(order);

        }

    }
    
}