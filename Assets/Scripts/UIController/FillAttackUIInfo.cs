﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ARCardGame.Models.Attacks;

namespace ARCardGame.UIControllers
{
    public class FillAttackUIInfo : MonoBehaviour
    {
        public GameObject attackUI;
        private const string UI_PREFIX = "UI";
        private const string NAME = "Name";
        private const string MANA = "Mana";
        private const string STAMINA = "Stamina";
        private const string PHYSICALDAMAGE = "PhysicalDamage";
        private const string MAGICDAMAGE = "MagicDamage";

        public GameObject FillAttackUI(AttackModel attack)
        {
            GameObject newAttackUI = Instantiate(attackUI) as GameObject;
            ChangeEditorName(attack, newAttackUI);
            ChangeNameValue(attack, newAttackUI);
            ChangeFieldValue(newAttackUI, attack.ManaPrice, MANA);
            ChangeFieldValue(newAttackUI, attack.StaminaPrice, STAMINA);
            ChangeFieldValue(newAttackUI, attack.PhysicalDamage, PHYSICALDAMAGE);
            ChangeFieldValue(newAttackUI, attack.MagicDamage, MAGICDAMAGE);
            return newAttackUI;
        }

        private static void ChangeNameValue(AttackModel attack, GameObject newAttackUI)
        {
            newAttackUI.transform.FindChild(NAME).GetComponent<Text>().text = attack.Name;
        }

        private static void ChangeFieldValue(GameObject newAttackUI, int value, string fieldName)
        {
            Text textValue = newAttackUI.transform.FindChild(fieldName).GetComponent<Text>();
            textValue.text = textValue.text + value;
        }

        private static void ChangeEditorName(AttackModel attack, GameObject newAttackUI)
        {
            newAttackUI.name = attack.Name + UI_PREFIX;
        }
    }
    
}