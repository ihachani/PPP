﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace ARCardGame.UIControllers
{
    public class AttackMenuController : MonoBehaviour
    {
        public GameObject AttacksList;
        CanvasGroup attackListCanvasGroup;
        CanvasGroup parentCanvasGroup;
        // Use this for initialization
        void Start()
        {
            attackListCanvasGroup = AttacksList.GetComponent<CanvasGroup>();
            parentCanvasGroup = GetComponent<CanvasGroup>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowAttacksList()
        {
            ActivateCanvasGroup(attackListCanvasGroup);
            DisableCanvasGroup(parentCanvasGroup);
        }
        private void ActivateCanvasGroup(CanvasGroup canvasGroup)
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
        }
        private void DisableCanvasGroup(CanvasGroup canvasGroup)
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }
        /// <summary>
        /// Shows OrderMenu and Hides Attack Menu.
        /// </summary>
        public void HideAttacksList()
        {
            //TODO rename this.
            ActivateCanvasGroup(parentCanvasGroup);
            DisableCanvasGroup(attackListCanvasGroup);
        }

        public void HideBoth()
        {
            DisableCanvasGroup(parentCanvasGroup);
            DisableCanvasGroup(attackListCanvasGroup);
        }
        public void ShowBoth()
        {
            ActivateCanvasGroup(parentCanvasGroup);
            ActivateCanvasGroup(attackListCanvasGroup);
        }
    } 
}
