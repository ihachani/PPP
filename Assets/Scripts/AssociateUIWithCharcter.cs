﻿using UnityEngine;
using System.Collections;
using ARCardGame.Models.Meters;
using ARCardGame.UIControllers;

namespace ARCardGame
{
    public class AssociateUIWithCharcter : MonoBehaviour
    {

        public MeterController HealthMeterController;
        public MeterController ManaMeterController;
        public MeterController StaminaMeterController;
        public AttacksListUICreator AttackListCreator;
        public void AssociateUI(GameObject charcter)
        {
            var metersList = charcter.GetComponent<MetersList>();
            AssociateMeter(metersList.HealthMeter, HealthMeterController);
            AssociateMeter(metersList.ManaMeter, ManaMeterController);
            AssociateMeter(metersList.StaminaMeter, StaminaMeterController);
            AttackListCreator.GenerateList(charcter);
        }

        private void AssociateMeter(IMeter meter, MeterController meterController)
        {
            meterController.InitializeMeter(meter);
        }
    }
    
}