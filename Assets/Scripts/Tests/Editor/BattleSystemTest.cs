﻿using System;
using NUnit.Framework;
using NSubstitute;
using ARCardGame.BattleSystem;
using ARCardGame.Models.Orders;
using ARCardGame.Models.MonstersStatus;
using ARCardGame.Models.Attacks;

namespace ARCardGame.Tests
{
    [TestFixture]
    public class BattleCalculationTest
    {
        IOrder ActorOrder;
        IOrder ReceiverOrder;
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            ActorOrder = Substitute.For<IOrder>();
            ReceiverOrder = Substitute.For<IOrder>();
        }

        [SetUp]
        public void Setup()
        {
            ActorOrder.MonsterStatus.PhysicalAttack.Returns(10);
            ActorOrder.MonsterStatus.MagicAttack.Returns(10);
            ActorOrder.MonsterStatus.PhysicalDefense.Returns(10);
            ActorOrder.MonsterStatus.MagicDefense.Returns(10);

            ReceiverOrder.MonsterStatus.PhysicalAttack.Returns(10);
            ReceiverOrder.MonsterStatus.MagicAttack.Returns(10);
            ReceiverOrder.MonsterStatus.PhysicalDefense.Returns(10);
            ReceiverOrder.MonsterStatus.MagicDefense.Returns(10);
            ReceiverOrder.DefenseState.Returns(false);
        }

        [Test]
        public void NormalBattlePhysical()
        {
            var ActorAttack = Substitute.For<IAttackModel>();
            ActorAttack.PhysicalDamage.Returns(2);
            ActorAttack.MagicDamage.Returns(0);
            ActorAttack.ManaPrice.Returns(0);
            ActorAttack.StaminaPrice.Returns(0);

            ActorOrder.Attack.Returns(ActorAttack);

            IOrderResult orderResult = BattleCalculation.HandleOrder(ActorOrder, ReceiverOrder);

            Assert.AreEqual(orderResult.TotalDamageDealt, 2);
     
        }
        [Test]
        public void NormalBattleMagical()
        {
            var ActorAttack = Substitute.For<IAttackModel>();
            ActorAttack.PhysicalDamage.Returns(0);
            ActorAttack.MagicDamage.Returns(2);
            ActorAttack.ManaPrice.Returns(0);
            ActorAttack.StaminaPrice.Returns(0);

            ActorOrder.Attack.Returns(ActorAttack);

            IOrderResult orderResult = BattleCalculation.HandleOrder(ActorOrder, ReceiverOrder);

            Assert.AreEqual(orderResult.TotalDamageDealt, 2);

        }

        [Test]
        public void NoDamageDealtBattle()
        {
            var ActorAttack = Substitute.For<IAttackModel>();
            ActorAttack.PhysicalDamage.Returns(0);
            ActorAttack.MagicDamage.Returns(0);
            ActorAttack.ManaPrice.Returns(0);
            ActorAttack.StaminaPrice.Returns(0);

            ActorOrder.Attack.Returns(ActorAttack);

            IOrderResult orderResult = BattleCalculation.HandleOrder(ActorOrder, ReceiverOrder);

            Assert.AreEqual(orderResult.TotalDamageDealt, 0);

        }

        [Test]
        public void PhysicalDefenseGreaterThanAttackBattle()
        {
            var ActorAttack = Substitute.For<IAttackModel>();
            ActorAttack.PhysicalDamage.Returns(1);
            ActorAttack.MagicDamage.Returns(0);
            ActorAttack.ManaPrice.Returns(0);
            ActorAttack.StaminaPrice.Returns(0);

            ActorOrder.Attack.Returns(ActorAttack);
            ActorOrder.MonsterStatus.PhysicalAttack.Returns(0);

            IOrderResult orderResult = BattleCalculation.HandleOrder(ActorOrder, ReceiverOrder);

            Assert.AreEqual(orderResult.TotalDamageDealt, 1);

        }
        [Test]
        public void MagicDefenseGreaterThanAttackBattle()
        {
            var ActorAttack = Substitute.For<IAttackModel>();
            ActorAttack.PhysicalDamage.Returns(0);
            ActorAttack.MagicDamage.Returns(1);
            ActorAttack.ManaPrice.Returns(0);
            ActorAttack.StaminaPrice.Returns(0);

            ActorOrder.Attack.Returns(ActorAttack);
            ActorOrder.MonsterStatus.MagicAttack.Returns(0);

            IOrderResult orderResult = BattleCalculation.HandleOrder(ActorOrder, ReceiverOrder);

            Assert.AreEqual(orderResult.TotalDamageDealt, 1);

        }
        [Test]
        public void ManaAndStaminaUsedTest()
        {
            var ActorAttack = Substitute.For<IAttackModel>();
            ActorAttack.ManaPrice.Returns(10);
            ActorAttack.StaminaPrice.Returns(10);

            ActorOrder.Attack.Returns(ActorAttack);
            ActorOrder.MonsterStatus.MagicAttack.Returns(0);

            IOrderResult orderResult = BattleCalculation.HandleOrder(ActorOrder, ReceiverOrder);

            Assert.AreEqual(orderResult.TotalManaLost, 10);
            Assert.AreEqual(orderResult.TotalStaminaLost, 10);
        }
    }
}
