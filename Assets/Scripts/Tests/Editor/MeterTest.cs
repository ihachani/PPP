﻿using UnityEngine;
using System.Collections;
using NUnit.Framework;
using NSubstitute;
using ARCardGame.Models.Meters;

namespace ARCardGame.Tests
{
    [TestFixture]
    public class MeterTest
    {
        Meter meter;
        [SetUp]
        public void Init()
        {
            meter = new Meter("AMeter", 100, null);
        }
        public interface IValueChangedMock
        {
            void MeterValueIncreased(object sender, MeterEventArgs eventArgs);
            void MeterValueDecreased(object sender, MeterEventArgs eventArgs);
            void MeterValueIS0(object sender, DeadEventArgs eventArgs);
        }
        /// <summary>
        /// Test For increase decrease value change.
        /// </summary>
        [Test]
        public void MeterValueChangesTest()
        {
            //var meter = new Meter("AMeter", 100, null);
            meter.Decrease(10);
            Assert.AreEqual(meter.CurrentValue, 90);
            meter.increase(5);
            Assert.AreEqual(meter.CurrentValue, 95);
        }
        /// <summary>
        /// Test for increasing more than the max value or decreasing more than 0.
        /// </summary>
        [Test]
        public void MeterDoNotSurpassMaxOrMinValue()
        {
            meter.increase(10);
            Assert.AreEqual(meter.CurrentValue, 100);
            meter.Decrease(150);
            Assert.AreEqual(meter.CurrentValue, 0);
        }
        /// <summary>
        /// Test for setValue Method.
        /// </summary>
        [Test]
        public void MeterSetValueTest()
        {
            meter.SetValue(10);
            Assert.AreEqual(meter.CurrentValue, 10);
            meter.SetValue(50);
            Assert.AreEqual(meter.CurrentValue, 50);
            meter.SetValue(150);
            Assert.AreEqual(meter.CurrentValue, 100);
            meter.SetValue(-20);
            Assert.AreEqual(meter.CurrentValue, 0);
        }
        /// <summary>
        /// Test For Increase And Decrease Events.
        /// </summary>
        [Test]
        public void MeterValueChangedEventRaised()
        {
            var receiver = Substitute.For<IValueChangedMock>();
            meter.ValueIncrease += receiver.MeterValueIncreased;
            meter.ValueDecrease += receiver.MeterValueDecreased;

            meter.Decrease(10);
            meter.increase(5);

            receiver.Received(1).MeterValueIncreased(meter, Arg.Is<MeterEventArgs>(value => value.CurrentValue == 95));
            receiver.Received(1).MeterValueDecreased(meter, Arg.Is<MeterEventArgs>(value => value.CurrentValue == 90));
        }
        /// <summary>
        /// Test For Value is 0 Event.
        /// </summary>
        [Test]
        public void MeterValueIs0EventRaised()
        {
            IValueChangedMock receiver = Substitute.For<IValueChangedMock>();
            meter.ValueIs0 += receiver.MeterValueIS0;

            meter.Decrease(100);

            receiver.Received(1).MeterValueIS0(meter, Arg.Is<DeadEventArgs>(value => value.DeadObject == null));
        }
        /// <summary>
        /// Test for events raised when using set value.
        /// </summary>
        [Test]
        public void MetetSetMethodEventsTest()
        {
            IValueChangedMock receiver = Substitute.For<IValueChangedMock>();
            meter.ValueIs0 += receiver.MeterValueIS0;
            meter.ValueIncrease += receiver.MeterValueIncreased;
            meter.ValueDecrease += receiver.MeterValueDecreased;

            meter.SetValue(10);
            meter.SetValue(95);
            meter.SetValue(0);

            Received.InOrder(() =>
            {
                receiver.MeterValueDecreased(meter, Arg.Is<MeterEventArgs>(value => value.CurrentValue == 10));
                receiver.MeterValueIncreased(meter, Arg.Is<MeterEventArgs>(value => value.CurrentValue == 95));
                receiver.MeterValueDecreased(meter, Arg.Is<MeterEventArgs>(value => value.CurrentValue == 0));
                receiver.MeterValueIS0(meter, Arg.Is<DeadEventArgs>(value => value.DeadObject == null));
            });
        }
    }
    
}