﻿using System;
using NUnit.Framework;
using NSubstitute;
using ARCardGame.DetectionSystem;

namespace ARCardGame.Tests
{
    /// <summary>
    /// Tests For ImageTarget Detection.
    /// </summary>
    [TestFixture]
    class DetectionTest
    {
        public interface IDetectionEventsMock
        {
            void OnObjectDetect(object sender, DetectionEventArgs eventArgs);
            void OnObjectLost(object sender, DetectionEventArgs eventArgs);
        }

        [Test]
        public void ObjectDetectedTest()
        {
            ICustomDetectionEventTrigger eventTrigger = new CustomDetectionEventImplementation();
            eventTrigger.GameObject = null;
            var receiver = Substitute.For<IDetectionEventsMock>();
            eventTrigger.ObjectDetect += receiver.OnObjectDetect;
            eventTrigger.ObjectLost += receiver.OnObjectLost;

            eventTrigger.OnTrackableStateChanged(Vuforia.TrackableBehaviour.Status.NOT_FOUND, Vuforia.TrackableBehaviour.Status.DETECTED);
            eventTrigger.OnTrackableStateChanged(Vuforia.TrackableBehaviour.Status.DETECTED, Vuforia.TrackableBehaviour.Status.NOT_FOUND);

            Received.InOrder(() =>
            {
                receiver.OnObjectDetect(eventTrigger, Arg.Is<DetectionEventArgs>(value => value.GameObject == null && value.DetectionStatus == true));
                receiver.OnObjectLost(eventTrigger, Arg.Is<DetectionEventArgs>(value => value.GameObject == null && value.DetectionStatus == false));
            }
            );

        }
    }
    
}