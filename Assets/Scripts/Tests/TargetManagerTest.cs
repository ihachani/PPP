﻿using UnityEngine;
using System.Collections;
using ARCardGame.DetectionSystem;
using ARCardGame.Managers;

namespace ARCardGame.Tests
{
    public class TargetManagerTest : MonoBehaviour
    {
        [SerializeField]
        private TargetManager targetManager;

        void Start()
        {
            var detectionEventHandler = GetComponent<CustomEventTrigger>();
            detectionEventHandler.OnTrackableStateChanged(Vuforia.TrackableBehaviour.Status.NOT_FOUND, Vuforia.TrackableBehaviour.Status.DETECTED);
        }

    }
    
}