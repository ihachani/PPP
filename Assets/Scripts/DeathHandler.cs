﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ARCardGame.Managers;
using ARCardGame.Models.Meters;

namespace ARCardGame
{
    public class DeathHandler : MonoBehaviour
    {
        public ITargetManager targetManager;
        public GameObject GameEndUI;
        // Use this for initialization
        void Start()
        {
            foreach (var gameObject in GameObject.FindGameObjectsWithTag("ImageTarget"))
            {
                GetHealthMeterOfImageTarget(gameObject).ValueIs0 += OnObjectDead;
            }
        }

        private static Meter GetHealthMeterOfImageTarget(GameObject gameObject)
        {
            return gameObject.transform.GetChild(0).gameObject.GetComponent<MetersList>().HealthMeter;
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnObjectDead(object sender, DeadEventArgs eventArgs)
        {

            GameObject deadObjectParent = eventArgs.DeadObject.transform.parent.gameObject;
            GameEndUI.SetActive(true);
            if (deadObjectParent == targetManager.Player1Monster)
            {
                GameEndUI.transform.GetChild(0).gameObject.GetComponent<Text>().text = "PLAYER 2 WINS!!!";
            }
            else
            {
                GameEndUI.transform.GetChild(0).gameObject.GetComponent<Text>().text = "PLAYER 1 WINS!!!";
            }
        }
    }
    
}